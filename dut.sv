typedef enum logic [3:0] {
  opADD=0,
  opSUB=1,
  opB=2,
  opLD=3,
  opST=4,
  opCBZ=5,
  opADDI=6,
  opANDI=7,
  opSUBI=8,
  opBL=9,
  opBR=10
} instrOpcode;

typedef enum logic [2:0] {
  aluADD=0,
  aluSUB=1,
  aluPASSFromInput1=2,
  aluOR=3,
  aluAND=4
} aluOp;

typedef enum logic [1:0] {
  branchNone=0,
  branchCB=1,
  branchB=2
} branchSel;

typedef struct packed {
  // Register control
  logic regWrite;
  logic regWriteIndexSel;
  logic regWriteDataSel;
  // ALU control
  logic aluSrc;
  aluOp aluSel;
  // Data Memory control
  logic memWrite;
  // PC Control
  branchSel branch;
} cpuControl;


module computer (
  output logic [15:0] progAddr,
  input logic [15:0] progInstr,
  input clk,
  input reset
);
  
// Register file
  logic [15:0] regFile [0:15];
  logic [15:0] readData1, readData2;
  logic [2:0] regWriteIndex;
  logic [15:0] regWriteData;
  
  always_comb
    begin
    if (progInstr[2:0]==7)
        readData1=0;
    else
      readData1=regFile[progInstr[2:0]];
    end
  
  always_comb
    begin
      if (progInstr[5:3]==7)
        readData2=0;
      else
        readData2=regFile[progInstr[5:3]];
    end
  
  always_comb
    case(ctl.regWriteIndexSel)
      0: regWriteIndex = progInstr[5:3];
      1: regWriteIndex = progInstr[8:6];
    endcase
  
  always_comb
    case(ctl.regWriteDataSel)
      0: regWriteData=aluResult;
      1: regWriteData=dmemReadData;
    endcase
        
  always_ff @(posedge clk)
    begin
      if (ctl.regWrite==1)
        regFile[regWriteIndex] <= regWriteData;
    end
        
// Data Memory
  logic [15:0] DMem [0:127];
  logic [15:0] dmemReadData;
  
  assign dmemReadData=DMem[aluResult[7:1]];
  
  always_ff @(posedge clk)
    if (ctl.memWrite==1)
      DMem[aluResult[7:1]]<=readData2;
  
// ALU
logic [15:0] aluResult;
logic [15:0] aluSrc2;
logic Zero;
  
always_comb
  case(ctl.aluSrc)
    0: aluSrc2 = readData2;
    1: aluSrc2 = {{10{progInstr[11]}},progInstr[11:6]};
  endcase
  
always_comb
  case(ctl.aluSel)
    aluADD: aluResult=readData1+aluSrc2;
    aluSUB: aluResult=readData1-aluSrc2;
    aluPASSFromInput1: aluResult=readData1;
    aluOR:  aluResult=readData1|aluSrc2;
    aluAND: aluResult=readData1&aluSrc2;
    default: aluResult=0;
  endcase

assign Zero=~|aluResult;
  
// PC Logic
  logic [15:0] pc;
  
  assign progAddr = pc;
  
  always_ff @(posedge clk)
    begin
    if (reset==1)
      pc <=0;
    else
      case(ctl.branch)
        branchNone: pc<=pc+2;
        branchCB: 
          begin
            if (Zero==1) pc<=pc+{{6{progInstr[11]}},progInstr[11:3],1'b0};
            else pc<=pc+2;
          end
        branchB: pc<=pc+{{3{progInstr[11]}},progInstr[11:0],1'b0};
        default: pc<=0;
      endcase
    end
  
// Controller
cpuControl ctl;
  
  always_comb
    case(progInstr[15:12])
      opADDI: 
        begin
          ctl.regWrite=1;
          ctl.regWriteIndexSel=0;
          ctl.aluSrc=1;
          ctl.aluSel=aluADD;
          ctl.memWrite=0;
          ctl.regWriteDataSel=0;
          ctl.branch=branchNone;
        end
      opB:
        begin
          ctl.regWrite=0;
          ctl.regWriteIndexSel=0;
          ctl.aluSrc=1;
          ctl.aluSel=aluADD;
          ctl.memWrite=0;
          ctl.regWriteDataSel=0;
          ctl.branch=branchB;
        end

      default: ctl.regWrite=0;
    endcase
  
endmodule
