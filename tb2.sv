
module tb;
  logic [15:0] addr;
  logic [15:0] instr;
  logic clk;
  logic reset;
  
  instrMemory im(addr,instr);
  computer cpu(addr,instr,clk,reset);
  
  initial
    begin
      clk=0;
      forever #1 clk=~clk;
    end
  
  initial
    begin
      $monitor("clk=%b reset=%b addr=%3d X0=%3d X1=%3d",clk,reset,addr,cpu.regFile[0],cpu.regFile[1]);
      reset=1;
      #2
      reset=0;
      #70 $finish;
    end
  
  
endmodule

module instrMemory(
  input logic [15:0] addr,
  output logic [15:0] instr
);
  
  function logic [15:0] rFormat
    (input instrOpcode op,
     input [2:0] regDst,
     input [2:0] regSrc1,
     input [2:0] regSrc2
    );
    rFormat={op, 3'b0, regDst,regSrc2,regSrc1};
  endfunction
  
  function logic [15:0] iFormat
    (input instrOpcode op,
     input [2:0] regDst,
     input [2:0] regSrc,
     input [5:0] immConst
    );
    iFormat={op, immConst, regDst,regSrc};
  endfunction
  
  function logic [15:0] dFormat
    (input instrOpcode op,
     input [2:0] regRegFile,
     input [2:0] regAddr,
     input [5:0] offsetConst
    );
    dFormat={op, offsetConst, regRegFile,regAddr};
  endfunction
  
  function logic [15:0] cbFormat
    (input instrOpcode op,
     input [2:0] regTest,
     input [8:0] offsetConst
    );
    cbFormat={op, offsetConst, regTest};
  endfunction
  
  function logic [15:0] bFormat
    (input instrOpcode op,
     input [11:0] offsetConst
    );
    bFormat={op, offsetConst};
  endfunction
  
  always_comb
    case(addr[4:1])
      // Loop1:
      0: instr=iFormat(opADDI,1,7,3);
      1: instr=rFormat(opADD,0,7,7);
      // Loop2:
      2: instr=cbFormat(opCBZ,1,-2);   // CBZ X1,Loop1
      3: instr=bFormat(opBL,4);        // BL Subr
      4: instr=bFormat(opBL,3);        // BL Subr
      5: instr=iFormat(opSUBI,1,1,1);
      6: instr=bFormat(opB,-4);        // B Loop2
      // Subr:
      7: instr=iFormat(opADDI,0,0,3);
      8: instr=cbFormat(opBR,6,0);
      default: instr=rFormat(opADD,0,0,0);
    endcase
  
endmodule




